Create schema MovieRent;
Use MovieRent;


create table Worker(
workerId int AUTO_INCREMENT PRIMARY KEY,
workerName varchar(20),
workerPassword varchar(20),
workerLastName varchar(20),
workerPhone varchar(12),
workerEmail varchar (25),
isManager bool
);

create table Movie (
movieID int AUTO_INCREMENT PRIMARY KEY,
movieName varchar(25),
movieActors varchar(65),
movieYear int,
movieDescription varchar(300),
movieQuantity int,
isTaken bool
);

create table Costumer(

costumerId int AUTO_INCREMENT PRIMARY KEY,
costumerName varchar(20),
costumerLastName varchar(20),
costumerPhone varchar(12),
costumerEmail varchar (25),
costumerAddress varchar (25),
costumerTakenMovies int,
FOREIGN KEY (costumerTakenMovies) REFERENCES Movie(movieID)
);