Create schema MovieRent;
Use MovieRent;

drop table Worker;

create table Worker(
workerID int AUTO_INCREMENT PRIMARY KEY,
workerName varchar(20),
workerLastName varchar(20),
workerPhone varchar(12),
workerEmail varchar (25)
);

create table Movie (
movieID int AUTO_INCREMENT PRIMARY KEY,
movieName varchar(25),
movieActors varchar(65),
movieYear int,
movieDescription varchar(300),
isAvailable bool
);

create table Customer(
customerID int AUTO_INCREMENT PRIMARY KEY,
customerName varchar(20),
customerLastName varchar(20),
customerPhone varchar(12),
customerEmail varchar (25),
customerAddress varchar (25)
);

create table tempCustomer(
customerID int AUTO_INCREMENT PRIMARY KEY,
customerName varchar(20),
customerLastName varchar(20)
);

create table CustomerOrder(
orderID int AUTO_INCREMENT PRIMARY KEY,
customerID int,
movieID int,
orderDate Date,
dueDate Date
);
