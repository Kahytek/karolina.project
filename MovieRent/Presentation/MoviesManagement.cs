﻿using System;
using System.Windows.Forms;
using Domain;
using Services;

namespace Presentation
{
    public partial class MoviesManagement : Form
    {
        public MovieViewModel _movie;
        private MovieViewModel _viewModel;
        private MoviesService _moviesService;
        public MoviesManagement()
        {
            _viewModel = new MovieViewModel();
            _moviesService = new MoviesService();
            InitializeComponent();
            CofigureGrid();
            FillDataGridView();
        }

        private void FillDataGridView()
        {
            try
            {
                var movies = _moviesService.GetMovies();
                moviesDgw.Rows.Clear();

                if (movies == null)
                {
                    throw new Exception("No data.");
                }

                foreach (var movie in movies)
                {
                    var rowIndex = moviesDgw.Rows.Add();
                    moviesDgw.Rows[rowIndex].Cells["ID"].Value = movie.MovieID;
                    moviesDgw.Rows[rowIndex].Cells["Title"].Value = movie.MovieName;
                    moviesDgw.Rows[rowIndex].Cells["Year"].Value = movie.MovieYear;
                    moviesDgw.Rows[rowIndex].Cells["Actors"].Value = movie.MovieActors;
                    moviesDgw.Rows[rowIndex].Cells["Description"].Value = movie.MovieDescription;
                    moviesDgw.Rows[rowIndex].Cells["IsAvailable"].Value = movie.IsAvailable;

                    //if (movie.IsAvailable == true)
                    //{
                    //    moviesDgw.CurrentRow.Cells["Description"].Value = true;
                    //}
                    //else { moviesDgw.CurrentRow.Cells["Description"].Value = false; }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "ID",
                HeaderText = "ID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(idColumn);

            DataGridViewTextBoxColumn titleColumn = new DataGridViewTextBoxColumn
            {
                Name = "Title",
                HeaderText = "Title",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(titleColumn);

            DataGridViewTextBoxColumn yearColumn = new DataGridViewTextBoxColumn
            {
                Name = "Year",
                HeaderText = "Year",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 85,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(yearColumn);

            DataGridViewTextBoxColumn actorsColumn = new DataGridViewTextBoxColumn
            {
                Name = "Actors",
                HeaderText = "MainActors",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 120,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(actorsColumn);

            DataGridViewTextBoxColumn descColumn = new DataGridViewTextBoxColumn
            {
                Name = "Description",
                HeaderText = "Description",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 190,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(descColumn);

            DataGridViewCheckBoxColumn availableColumn = new DataGridViewCheckBoxColumn
            {
                Name = "IsAvailable",
                HeaderText = "IsAvailable",
                CellTemplate = new System.Windows.Forms.DataGridViewCheckBoxCell(),
                Width = 60,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(availableColumn);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            moviesDgw.Columns.Add(deleteButton);
        }

        private void Clear()
        {
            nameTxt.Text = yearTxt.Text = actorsTxt.Text = descTxt.Text = idTxt.Text = searchTxt.Text = "";
            availableChbx.Checked = false;
        }

        private void moviesDgw_DoubleClick(object sender, EventArgs e)
        {
            if (moviesDgw.CurrentRow.Index != -1)
            {
                idTxt.Text = moviesDgw.CurrentRow.Cells[0].Value.ToString();
                nameTxt.Text = moviesDgw.CurrentRow.Cells[1].Value.ToString();
                yearTxt.Text = moviesDgw.CurrentRow.Cells[2].Value.ToString();
                actorsTxt.Text = moviesDgw.CurrentRow.Cells[3].Value.ToString();
                descTxt.Text = moviesDgw.CurrentRow.Cells[4].Value.ToString();
                if (Convert.ToBoolean(moviesDgw.CurrentRow.Cells[5].Value))
                {
                    availableChbx.Checked = true;
                }
                else { availableChbx.Checked = false; }

                newBtn.Text = "UPDATE";
            }
        }

        private void newBtn_Click(object sender, EventArgs e)
        {
            if (newBtn.Text == "UPDATE")
            {
                _movie = new MovieViewModel();
                _movie.MovieID = int.Parse(idTxt.Text);
                _movie.MovieName = nameTxt.Text;
                _movie.MovieYear = int.Parse(yearTxt.Text);
                _movie.MovieActors = actorsTxt.Text;
                _movie.MovieDescription = descTxt.Text;
                if (_movie.IsAvailable == true)
                { availableChbx.Checked = true; }
                else { availableChbx.Checked = false; }

                _moviesService.UpdateMovie(_movie);
                FillDataGridView();
                MessageBox.Show("Update successful");
                newBtn.Text = "NEW";
                Clear();
            }
            else if (newBtn.Text == "NEW")
            {
                _viewModel.MovieName = nameTxt.Text;
                _viewModel.MovieYear = int.Parse(yearTxt.Text);
                _viewModel.MovieActors = actorsTxt.Text;
                _viewModel.MovieDescription = descTxt.Text;

                _moviesService.CreateMovie(_viewModel);
                FillDataGridView();
                Clear();
            }
            
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            moviesDgw.Rows.Clear();

            var searchMovies = _moviesService.SearchMovies(searchTxt.Text);
            if (searchMovies != null)
            {
                foreach (var movie in searchMovies)
                {
                    var rowIndex = moviesDgw.Rows.Add();
                    moviesDgw.Rows[rowIndex].Cells["ID"].Value = movie.MovieID;
                    moviesDgw.Rows[rowIndex].Cells["Title"].Value = movie.MovieName;
                    moviesDgw.Rows[rowIndex].Cells["Year"].Value = movie.MovieYear;
                    moviesDgw.Rows[rowIndex].Cells["Actors"].Value = movie.MovieActors;
                    moviesDgw.Rows[rowIndex].Cells["Description"].Value = movie.MovieDescription;
                    moviesDgw.Rows[rowIndex].Cells["IsAvailable"].Value = movie.IsAvailable;
                }
            }
            else { MessageBox.Show("Nothing found"); FillDataGridView(); }   
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Clear();
            FillDataGridView();
            newBtn.Text = "NEW";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Customers customers = new Customers();
            customers.Location = this.Location;
            customers.Show();
        }

        private void moviesDgw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                idTxt.Text = moviesDgw.CurrentRow.Cells[0].Value.ToString();
                DialogResult dialogResult = MessageBox.Show("Are you sure want to delete this movie?", "Delete movie", MessageBoxButtons.YesNo);
                

                if (dialogResult == DialogResult.Yes)
                {
                    _moviesService.DeleteMovie(int.Parse(idTxt.Text));
                    FillDataGridView();
                    Clear();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WorkersManagement workers = new WorkersManagement();
            workers.Show();
        }
    }
}
