﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
using Services;
using MySql.Data.MySqlClient;

namespace Presentation
{
    public partial class Order : Form
    {
        private OrderViewModel _order;
        private CustomerViewModel _customer;
        private OrderService _orderService;
        private CostumerService _CustomerService;
        public Order(CustomerViewModel customers)
        {
            _orderService = new OrderService();
            _customer = customers;
            _CustomerService = new CostumerService();
            InitializeComponent();
            FillForm();
            CofigureGrid();
            FillDataGridView();
        }

        private void FillForm()
        {
            var customers = _CustomerService.GetCustomer();

            if (_customer.CustomerID != 0)
            {
                foreach (var customer in customers)
                {
                    customerIdTxt.Text = _customer.CustomerID.ToString();
                    customerNameTxt.Text = _customer.CustomerName.ToString();
                    lastNameTxt.Text = _customer.CustomerLastName.ToString();
                }
                _CustomerService.DeleteCustomer(int.Parse(customerIdTxt.Text));
            }
        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "ID",
                HeaderText = "ID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(idColumn);

            DataGridViewTextBoxColumn customerIDColumn = new DataGridViewTextBoxColumn
            {
                Name = "CustomerID",
                HeaderText = "CustomerID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(customerIDColumn);

            DataGridViewTextBoxColumn movieIDColumn = new DataGridViewTextBoxColumn
            {
                Name = "MovieID",
                HeaderText = "MovieID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(movieIDColumn);

            DataGridViewTextBoxColumn movieNameColumn = new DataGridViewTextBoxColumn
            {
                Name = "MovieName",
                HeaderText = "Movie name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 150,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(movieNameColumn);

            DataGridViewTextBoxColumn orderDateColumn = new DataGridViewTextBoxColumn
            {
                Name = "OrderDate",
                HeaderText = "OrderDate",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(orderDateColumn);

            DataGridViewTextBoxColumn dueDateColumn = new DataGridViewTextBoxColumn
            {
                Name = "DueDate",
                HeaderText = "DueDate",
                CellTemplate = new System.Windows.Forms.DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            ordersDgw.Columns.Add(dueDateColumn);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            ordersDgw.Columns.Add(deleteButton);

        }

        private void FillDataGridView()
        {
            try
            {
                var customers = _orderService.GetOrders();
                
                ordersDgw.Rows.Clear();

                if (_order.CustomerID != 0)
                {
                    customerIdTxt.Text = _order.CustomerID.ToString();
                }

                foreach (var customer in customers)
                {
                    if(customer.CustomerID.ToString() == customerIdTxt.Text)
                    {
                        var rowIndex = ordersDgw.Rows.Add();
                        ordersDgw.Rows[rowIndex].Cells["ID"].Value = customer.CustomerID;
                        ordersDgw.Rows[rowIndex].Cells["CustomerID"].Value = customer.CustomerID;
                        ordersDgw.Rows[rowIndex].Cells["MovieID"].Value = customer.MovieID;
                        string movieName = _orderService.GetMovieName(customer.MovieID);
                        ordersDgw.Rows[rowIndex].Cells["MovieName"].Value = movieName;
                        ordersDgw.Rows[rowIndex].Cells["OrderDate"].Value = customer.OrderDate;
                        ordersDgw.Rows[rowIndex].Cells["DueDate"].Value = customer.DueDate;
                    }                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void moviesDgw_DoubleClick(object sender, EventArgs e)
        {
            if (ordersDgw.CurrentRow.Index != -1)
            {
                customerIdTxt.Text = ordersDgw.CurrentRow.Cells[0].Value.ToString();
                orderDateTxt.Text = ordersDgw.CurrentRow.Cells[3].Value.ToString();
                dateDueTxt.Text = ordersDgw.CurrentRow.Cells[4].Value.ToString();
                var id = Convert.ToInt32(ordersDgw.CurrentRow.Cells[2].Value.ToString());
                nameTxt.Text = _orderService.GetMovieName(id);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Customers customers = new Customers();
            customers.Show();
        }

        private void newBtn_Click(object sender, EventArgs e)
        {
            var addOrder = new AddOrder(customerIdTxt.Text);
            addOrder.Show();
        }

        private void ordersDgw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to delete this customer?", "Delete customer", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    _orderService.DeleteOrder(int.Parse(ordersDgw.CurrentRow.Cells[0].Value.ToString()));
                    FillDataGridView();
                }
            }
        }
    }
}
