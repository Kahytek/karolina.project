﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
using Services;

namespace Presentation
{
    public partial class WorkersManagement : Form
    {

        private WorkerViewModel _worker;
        private WorkerService _workerService;
        public WorkersManagement()
        {
            _worker = new WorkerViewModel();
            _workerService = new WorkerService();
            InitializeComponent();
            CofigureGrid();
            FillDataGridView();
        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "ID",
                HeaderText = "ID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            workerDgw.Columns.Add(idColumn);

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn
            {
                Name = "Name",
                HeaderText = "Name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            workerDgw.Columns.Add(nameColumn);

            DataGridViewTextBoxColumn lastNameColumn = new DataGridViewTextBoxColumn
            {
                Name = "LastName",
                HeaderText = "Last Name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            workerDgw.Columns.Add(lastNameColumn);

            DataGridViewTextBoxColumn phoneColumn = new DataGridViewTextBoxColumn
            {
                Name = "Phone",
                HeaderText = "Phone number",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 150,
                Resizable = DataGridViewTriState.False
            };
            workerDgw.Columns.Add(phoneColumn);

            DataGridViewTextBoxColumn emailColumn = new DataGridViewTextBoxColumn
            {
                Name = "Email",
                HeaderText = "Email",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 120,
                Resizable = DataGridViewTriState.False
            };
            workerDgw.Columns.Add(emailColumn);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            workerDgw.Columns.Add(deleteButton);
        }

        private void FillDataGridView()
        {
            try
            {
                var workers = _workerService.GetWorkers ();
                workerDgw.Rows.Clear();

                if (workers == null)
                {
                    throw new Exception("No data.");
                }

                foreach (var worker in workers)
                {
                    var rowIndex = workerDgw.Rows.Add();
                    workerDgw.Rows[rowIndex].Cells["ID"].Value = worker.WorkerID;
                    workerDgw.Rows[rowIndex].Cells["Name"].Value = worker.WorkerName;
                    workerDgw.Rows[rowIndex].Cells["LastName"].Value = worker.WorkerLastName;
                    workerDgw.Rows[rowIndex].Cells["Phone"].Value = worker.WorkerPhone;
                    workerDgw.Rows[rowIndex].Cells["Email"].Value = worker.WorkerEmail;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void NewBtn_Click(object sender, EventArgs e)
        {
            if (newBtn.Text == "UPDATE")
            {
                _worker = new WorkerViewModel();
                _worker.WorkerID = int.Parse(idTxt.Text);
                _worker.WorkerName = nameTxt.Text;
                _worker.WorkerLastName  = lastNameTxt.Text;
                _worker.WorkerPhone = phoneTxt.Text;
                _worker.WorkerEmail = emailTxt.Text;

                _workerService.UpdateWorker(_worker);
                FillDataGridView();
                MessageBox.Show("Update successful");
                newBtn.Text = "NEW";
                Clear();
            }
            else if (newBtn.Text == "NEW")
            {
                _worker = new WorkerViewModel();         
                _worker.WorkerName = nameTxt.Text;
                _worker.WorkerLastName = lastNameTxt.Text;
                _worker.WorkerPhone = phoneTxt.Text;
                _worker.WorkerEmail = emailTxt.Text;

                _workerService.CreateWorker(_worker);
                FillDataGridView();
                Clear();
            }
        }

        private void Clear()
        {
            nameTxt.Text = lastNameTxt.Text = phoneTxt.Text = emailTxt.Text = idTxt.Text = searchTxt.Text = "";
        }

        private void workerDgw_DoubleClick(object sender, EventArgs e)
        {
            if (workerDgw.CurrentRow.Index != -1)
            {
                idTxt.Text = workerDgw.CurrentRow.Cells[0].Value.ToString();
                nameTxt.Text = workerDgw.CurrentRow.Cells[1].Value.ToString();
                lastNameTxt.Text = workerDgw.CurrentRow.Cells[2].Value.ToString();
                phoneTxt.Text = workerDgw.CurrentRow.Cells[3].Value.ToString();
                emailTxt.Text = workerDgw.CurrentRow.Cells[4].Value.ToString();

                newBtn.Text = "UPDATE";
            }
        }

        private void workerDgw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            { 
                DialogResult dialogResult = MessageBox.Show("Are you sure want to delete this worker?", "Delete worker", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    _workerService.DeleteWorker(int.Parse(workerDgw.CurrentRow.Cells[0].Value.ToString()));
                    FillDataGridView();
                    Clear();
                }
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            workerDgw.Rows.Clear();

            var searchWorkers = _workerService.SearchWorkers(searchTxt.Text);
            if (searchWorkers != null)
            {
                foreach (var worker in searchWorkers)
                {
                    var rowIndex = workerDgw.Rows.Add();
                    workerDgw.Rows[rowIndex].Cells["ID"].Value = worker.WorkerID;
                    workerDgw.Rows[rowIndex].Cells["Name"].Value = worker.WorkerName;
                    workerDgw.Rows[rowIndex].Cells["LastName"].Value = worker.WorkerLastName;
                    workerDgw.Rows[rowIndex].Cells["Phone"].Value = worker.WorkerPhone;
                    workerDgw.Rows[rowIndex].Cells["Email"].Value = worker.WorkerEmail;
                }
            }
            else { MessageBox.Show("Nothing found"); FillDataGridView(); }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Clear();
            FillDataGridView();
            newBtn.Text = "NEW";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Customers customers = new Customers();
            customers.Location = this.Location;
            customers.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MoviesManagement movies = new MoviesManagement();
            movies.Location = this.Location;
            movies.Show();
        }
    }

}
