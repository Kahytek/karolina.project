﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
using Services;

namespace Presentation
{

    public partial class AddOrder : Form
    {
        private OrderViewModel _order;
        private OrderService _orderService;
        private MoviesService _movieSercice;
        string customerId;
        public AddOrder(string customerID)
        {
            _movieSercice = new MoviesService();
            _order = new OrderViewModel();
            _orderService = new OrderService();
            customerId = customerID;
            InitializeComponent();
            CofigureGrid();
            FillForm();
            FillDataGridView();
        }

        private void FillForm()
        {
            orderDateTxt.Text = DateTime.Now.ToString();
            DateTime newDate = DateTime.Parse(orderDateTxt.Text).AddDays(14);
            dueDateTxt.Text = newDate.ToString();
            customerIdTxt.Text = customerId;
        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "ID",
                HeaderText = "ID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(idColumn);

            DataGridViewTextBoxColumn titleColumn = new DataGridViewTextBoxColumn
            {
                Name = "Title",
                HeaderText = "Title",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(titleColumn);

            DataGridViewTextBoxColumn yearColumn = new DataGridViewTextBoxColumn
            {
                Name = "Year",
                HeaderText = "Year",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 85,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(yearColumn);

            DataGridViewTextBoxColumn actorsColumn = new DataGridViewTextBoxColumn
            {
                Name = "Actors",
                HeaderText = "MainActors",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 120,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(actorsColumn);

            DataGridViewTextBoxColumn descColumn = new DataGridViewTextBoxColumn
            {
                Name = "Description",
                HeaderText = "Description",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 190,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(descColumn);

            DataGridViewCheckBoxColumn availableColumn = new DataGridViewCheckBoxColumn
            {
                Name = "IsAvailable",
                HeaderText = "IsAvailable",
                CellTemplate = new System.Windows.Forms.DataGridViewCheckBoxCell(),
                Width = 60,
                Resizable = DataGridViewTriState.False
            };
            moviesDgw.Columns.Add(availableColumn);
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
                _order.CustomerID = int.Parse(customerIdTxt.Text);
                _order.MovieID = int.Parse(movieIDTxt.Text);
                _order.OrderDate = DateTime.Parse(orderDateTxt.Text);
                _order.DueDate = DateTime.Parse(dueDateTxt.Text);

                _orderService.CreateOrder(_order);
                MessageBox.Show("Order created");
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            moviesDgw.Rows.Clear();

            var searchMovies = _movieSercice.SearchMovies(searchTxt.Text);
            foreach (var movie in searchMovies)
            {
                var rowIndex = moviesDgw.Rows.Add();
                moviesDgw.Rows[rowIndex].Cells["MovieID"].Value = movie.MovieID;
                moviesDgw.Rows[rowIndex].Cells["MovieName"].Value = movie.MovieName;
                moviesDgw.Rows[rowIndex].Cells["MovieActors"].Value = movie.MovieActors;
                moviesDgw.Rows[rowIndex].Cells["MovieYear"].Value = movie.MovieYear;
                moviesDgw.Rows[rowIndex].Cells["MovieDescription"].Value = movie.MovieDescription;
                moviesDgw.Rows[rowIndex].Cells["IsAvailable"].Value = movie.IsAvailable;
            }
        }

        private void FillDataGridView()
        {
            try
            {
                var movies = _movieSercice.GetMovies();
                moviesDgw.Rows.Clear();

                if (movies == null)
                {
                    throw new Exception("No data.");
                }

                foreach (var movie in movies)
                {
                    var rowIndex = moviesDgw.Rows.Add();
                    moviesDgw.Rows[rowIndex].Cells["ID"].Value = movie.MovieID;
                    moviesDgw.Rows[rowIndex].Cells["Title"].Value = movie.MovieName;
                    moviesDgw.Rows[rowIndex].Cells["Year"].Value = movie.MovieYear;
                    moviesDgw.Rows[rowIndex].Cells["Actors"].Value = movie.MovieActors;
                    moviesDgw.Rows[rowIndex].Cells["Description"].Value = movie.MovieDescription;
                    moviesDgw.Rows[rowIndex].Cells["IsAvailable"].Value = movie.IsAvailable;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void moviesDgw_DoubleClick(object sender, EventArgs e)
        {
            if (moviesDgw.CurrentRow.Index != -1)
            {
                movieIDTxt.Text = moviesDgw.CurrentRow.Cells[0].Value.ToString();
            }
        }
    }
}
