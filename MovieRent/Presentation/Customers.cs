﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domain;
using Services;

namespace Presentation
{
    public partial class Customers : Form
    {
        private CustomerViewModel _customer;
        private CostumerService _customerService;
        public Customers()
        {
            _customerService = new CostumerService();
            _customer = new CustomerViewModel();
            InitializeComponent();
            CofigureGrid();
            FillDataGridView();
        }

        private void CofigureGrid()
        {
            DataGridViewTextBoxColumn idColumn = new DataGridViewTextBoxColumn
            {
                Name = "ID",
                HeaderText = "ID",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 20,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(idColumn);

            DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn
            {
                Name = "Name",
                HeaderText = "Name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 87,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(nameColumn);

            DataGridViewTextBoxColumn lastNameColumn = new DataGridViewTextBoxColumn
            {
                Name = "LastName",
                HeaderText = "Last name",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 88,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(lastNameColumn);

            DataGridViewTextBoxColumn phoneColumn = new DataGridViewTextBoxColumn
            {
                Name = "Phone",
                HeaderText = "PhoneNumber",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(phoneColumn);

            DataGridViewTextBoxColumn emailColumn = new DataGridViewTextBoxColumn
            {
                Name = "Email",
                HeaderText = "Email",
                CellTemplate = new DataGridViewTextBoxCell(),
                Width = 130,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(emailColumn);

            DataGridViewTextBoxColumn addressColumn = new DataGridViewTextBoxColumn
            {
                Name = "Address",
                HeaderText = "Address",
                CellTemplate = new System.Windows.Forms.DataGridViewTextBoxCell(),
                Width = 100,
                Resizable = DataGridViewTriState.False
            };
            customerDgw.Columns.Add(addressColumn);

            DataGridViewButtonColumn deleteButton = new DataGridViewButtonColumn
            {
                Name = "Delete",
                HeaderText = "Delete",
                UseColumnTextForButtonValue = true,
                Text = "Delete",
                CellTemplate = new DataGridViewButtonCell(),
                Resizable = DataGridViewTriState.False,
                SortMode = DataGridViewColumnSortMode.NotSortable
            };
            customerDgw.Columns.Add(deleteButton);
        }

        private void Clear()
        {
            nameTxt.Text = lastNameTxt.Text = phoneTxt.Text = emailTxt.Text = addressTxt.Text = searchTxt.Text = "";
        }

        private void FillDataGridView()
        {
            try
            {
                var customers = _customerService.GetCustomer();
                customerDgw.Rows.Clear();

                if (customers ==  null)
                {
                    throw new Exception("No data found.");
                }

                foreach (var customer in customers)
                {
                    var rowIndex = customerDgw.Rows.Add();
                    customerDgw.Rows[rowIndex].Cells["ID"].Value = customer.CustomerID;
                    customerDgw.Rows[rowIndex].Cells["Name"].Value = customer.CustomerName;
                    customerDgw.Rows[rowIndex].Cells["LastName"].Value = customer.CustomerLastName;
                    customerDgw.Rows[rowIndex].Cells["Phone"].Value = customer.CustomerPhone;
                    customerDgw.Rows[rowIndex].Cells["Email"].Value = customer.CustomerEmail;
                    customerDgw.Rows[rowIndex].Cells["Address"].Value = customer.CustomerAddress;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }     

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Clear();
            FillDataGridView();
            newBtn.Text = "NEW";
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            customerDgw.Rows.Clear();

            var searchCustomer = _customerService.SearchCustomer(searchTxt.Text);
            if (searchCustomer != null)
            {
                foreach (var customer in searchCustomer)
                {
                    var rowIndex = customerDgw.Rows.Add();
                    customerDgw.Rows[rowIndex].Cells["ID"].Value = customer.CustomerID;
                    customerDgw.Rows[rowIndex].Cells["Name"].Value = customer.CustomerName;
                    customerDgw.Rows[rowIndex].Cells["LastName"].Value = customer.CustomerLastName;
                    customerDgw.Rows[rowIndex].Cells["Phone"].Value = customer.CustomerPhone;
                    customerDgw.Rows[rowIndex].Cells["Email"].Value = customer.CustomerEmail;
                    customerDgw.Rows[rowIndex].Cells["Address"].Value = customer.CustomerAddress;
                }
            }
            else { MessageBox.Show("Nothing found"); FillDataGridView(); }

        }

        private void customerDgw_DoubleClick(object sender, EventArgs e)
        {
            if (customerDgw.CurrentRow.Index != -1)
            {
                idTxt.Text = customerDgw.CurrentRow.Cells[0].Value.ToString();
                nameTxt.Text = customerDgw.CurrentRow.Cells[1].Value.ToString();
                lastNameTxt.Text = customerDgw.CurrentRow.Cells[2].Value.ToString();
                phoneTxt.Text = customerDgw.CurrentRow.Cells[3].Value.ToString();
                emailTxt.Text = customerDgw.CurrentRow.Cells[4].Value.ToString();
                addressTxt.Text = customerDgw.CurrentRow.Cells[5].Value.ToString();
        
                newBtn.Text = "UPDATE";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MoviesManagement movies = new MoviesManagement();
            movies.Location = this.Location;
            movies.Show(); 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WorkersManagement workers = new WorkersManagement();
            workers.Show();
        }

        private void newBtn_Click_1(object sender, EventArgs e)
        {
            if (newBtn.Text == "UPDATE")
            {
                _customer.CustomerID = int.Parse(idTxt.Text);
                _customer.CustomerName = nameTxt.Text;
                _customer.CustomerLastName = lastNameTxt.Text;
                _customer.CustomerPhone = phoneTxt.Text;
                _customer.CustomerEmail = emailTxt.Text;
                _customer.CustomerAddress = addressTxt.Text;

                _customerService.UpdateCustomer(_customer);
                FillDataGridView();
                MessageBox.Show("Update successful");
                newBtn.Text = "NEW";
                Clear();
            }
            else if (newBtn.Text == "NEW")
            {
                _customer.CustomerName = nameTxt.Text;
                _customer.CustomerLastName = lastNameTxt.Text;
                _customer.CustomerPhone = phoneTxt.Text;
                _customer.CustomerEmail = emailTxt.Text;
                _customer.CustomerAddress = addressTxt.Text;

                _customerService.CreateCustomer(_customer);
                FillDataGridView();
            }
        }

        private void customerDgw_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 6)
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure want to delete this customer?", "Delete customer", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    _customerService.DeleteCustomer(int.Parse(customerDgw.CurrentRow.Cells[0].Value.ToString()));
                    FillDataGridView();
                }
            }
        }

        private void ordersBtn_Click(object sender, EventArgs e)
        {
            try
            {
                _customer.CustomerID = int.Parse(idTxt.Text);
                _customer.CustomerName = nameTxt.Text;
                _customer.CustomerLastName = lastNameTxt.Text;

                Order order = new Order(_customer);
                order.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please choose a customer to view his orders");

            }
                
        }

    }
}
