﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class OrderViewModel
    {
        public int OrderID { get; set; }

        public int CustomerID { get; set; }

        public int MovieID { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime DueDate { get; set; }
    }
}
