﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class WorkerViewModel
    {
        public int? WorkerID { get; set; }

        public string WorkerName { get; set; }

        public string WorkerLastName { get; set; }

        public string WorkerPhone { get; set; }

        public string WorkerEmail { get; set; } 
    }
}
