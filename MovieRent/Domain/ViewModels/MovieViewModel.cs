﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class MovieViewModel
    {
        public int? MovieID { get; set; }

        public string MovieName { get; set; }

        public string MovieActors { get; set; }

        public int MovieYear { get; set; }

        public string MovieDescription { get; set; }

        public bool IsAvailable { get; set; }
    }
}
