﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
   public class BaseService : DbConnection
    {

        protected void OpenConnection()
        {
            if (movConnection.State == System.Data.ConnectionState.Closed)
            {
                movConnection.Open();
            }
        }

        protected void CloseConnection()
        {
            if ( movConnection.State == System.Data.ConnectionState.Open)
            {
                movConnection.Close();
            }
        }
    }
}
