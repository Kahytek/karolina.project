﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;
using MySql.Data.MySqlClient;

namespace Services
{
    public class OrderService : BaseService
    {
        public OrderViewModel CreateOrder(OrderViewModel viewModel)
        {
            var CreateOrderCmdText = "INSERT INTO CustomerOrder (orderID, customerID, movieID, orderDate, dueDate)" +
                " VALUES (@orderID, @customerID, @movieID, @orderDate, @dueDate) ";

            var CreateMovieCmd = new MySqlCommand(CreateOrderCmdText, movConnection);
            CreateMovieCmd.Parameters.AddWithValue("@orderID", viewModel.OrderID);
            CreateMovieCmd.Parameters.AddWithValue("@customerID", viewModel.CustomerID);
            CreateMovieCmd.Parameters.AddWithValue("@movieID", viewModel.MovieID);
            CreateMovieCmd.Parameters.AddWithValue("@orderDate", viewModel.OrderDate);
            CreateMovieCmd.Parameters.AddWithValue("@dueDate", viewModel.DueDate);

            OpenConnection();
            CreateMovieCmd.ExecuteNonQuery();
            CloseConnection();

            return viewModel;
        }

        public List<OrderViewModel> GetOrders()
        {

            List<OrderViewModel> orders = null;
            var ordersCmdText = "SELECT * FROM movie;";
            var ordersCmd = new MySqlCommand(ordersCmdText, movConnection);

            OpenConnection();
            var reader = ordersCmd.ExecuteReader();

            if (reader.HasRows)
            {
                orders = new List<OrderViewModel>();
            }

            while (reader.Read())
            {
                orders.Add(new OrderViewModel
                {
                    OrderID = reader.GetInt32("OrderID"),
                    CustomerID = reader.GetInt32("CustomerID"),
                    MovieID = reader.GetInt32("MovieID"),
                    OrderDate = reader.GetDateTime("OrderDate"),
                    DueDate = reader.GetDateTime("DueDate")
                });
            }
            CloseConnection();
            return orders;
        }

        public string GetMovieName(int id)
        {
            string MovieName = null;
            var getNameCmdText = "Select movieName FROM movie WHERE movieID = @movieID";

            var getNameCmd = new MySqlCommand(getNameCmdText, movConnection);
            getNameCmd.Parameters.AddWithValue("@movieID", id);

            OpenConnection();
            var reader = getNameCmd.ExecuteReader();

            while (reader.Read())
            {
                MovieName = reader.GetString("movieName");
            }

            CloseConnection();
            return MovieName;
        }

        public int DeleteOrder(int ID)
        {
            var deleteOrderCmdText = "DELETE FROM CustomerOrder where OrderID = @OrderID";
            var deleteOrderCmd = new MySqlCommand(deleteOrderCmdText, movConnection);

            //Parameters
            deleteOrderCmd.Parameters.AddWithValue("@OrderID", ID);

            OpenConnection();

            return deleteOrderCmd.ExecuteNonQuery();
        }
    }
}
