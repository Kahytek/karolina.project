﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Services
{
    public class MoviesService : BaseService
    {
        public MovieViewModel CreateMovie(MovieViewModel viewModel)
        { 
                var movieAddCmdText = "INSERT INTO Movie (MovieID, MovieName, MovieActors, MovieYear, MovieDescription, IsAvailable) VALUES (@MovieID, " +
                    "@MovieName, @MovieActors, @MovieYear, @MovieDescription, @IsAvailable);";

                var movieAddCmd = new MySqlCommand(movieAddCmdText, movConnection);
                movieAddCmd.Parameters.AddWithValue("@MovieID", viewModel.MovieID);
                movieAddCmd.Parameters.AddWithValue("@MovieName", viewModel.MovieName);
                movieAddCmd.Parameters.AddWithValue("@MovieActors", viewModel.MovieActors);
                movieAddCmd.Parameters.AddWithValue("@MovieYear", viewModel.MovieYear);
                movieAddCmd.Parameters.AddWithValue("@MovieDescription", viewModel.MovieDescription);
                movieAddCmd.Parameters.AddWithValue("@IsAvailable", viewModel.IsAvailable);

                OpenConnection();
                movieAddCmd.ExecuteNonQuery();
                CloseConnection();
            
            return viewModel;
        }

        public List<MovieViewModel> GetMovies()
        {
            List<MovieViewModel> movies = null;
            var moviesCmdText = "SELECT * FROM movie;";
            var moviesCmd = new MySqlCommand(moviesCmdText, movConnection);

            OpenConnection();
            var reader = moviesCmd.ExecuteReader();

            if (reader.HasRows)
            {
                movies = new List<MovieViewModel>();
            }

            while (reader.Read())
            {
                movies.Add(new MovieViewModel
                {
                    MovieID = reader.GetInt32("MovieID"),
                    MovieName = reader.GetString("MovieName"),
                    MovieActors = reader.GetString("MovieActors"),
                    MovieYear = reader.GetInt32("MovieYear"),
                    MovieDescription = reader.GetString("MovieDescription"),
                    IsAvailable = reader.GetBoolean("IsAvailable")
                });
            }
            CloseConnection();
            return movies;
        }

        public MovieViewModel UpdateMovie(MovieViewModel viewModel)
        {
            var movieUpdateCmdText = "UPDATE movie SET MovieName = @MovieName, MovieActors = @MovieActors, MovieYear = @MovieYear, " +
                "MovieDescription = @MovieDescription, IsAvailable = @IsAvailable WHERE MovieID = @MovieID;";

            var movieUpdateCmd = new MySqlCommand(movieUpdateCmdText, movConnection);
            movieUpdateCmd.Parameters.AddWithValue("@MovieName", viewModel.MovieName);
            movieUpdateCmd.Parameters.AddWithValue("@MovieActors", viewModel.MovieActors);
            movieUpdateCmd.Parameters.AddWithValue("@MovieYear", viewModel.MovieYear);
            movieUpdateCmd.Parameters.AddWithValue("@MovieDescription", viewModel.MovieDescription);
            movieUpdateCmd.Parameters.AddWithValue("@IsAvailable", viewModel.IsAvailable);
            movieUpdateCmd.Parameters.AddWithValue("@MovieID", viewModel.MovieID);

            OpenConnection();
            movieUpdateCmd.ExecuteNonQuery();
            CloseConnection();
            return viewModel;
        }

        public int DeleteMovie(int ID)
        {
            var deleteMovieCmdText = "DELETE FROM movie where MovieID = @MovieID";
            var deleteMovieCmd = new MySqlCommand(deleteMovieCmdText, movConnection);

            //Parameters
            deleteMovieCmd.Parameters.AddWithValue("@MovieID", ID);

            OpenConnection();

            return deleteMovieCmd.ExecuteNonQuery();
        }

        public List<MovieViewModel> SearchMovies(string valueToSearch)
        {
            List<MovieViewModel> searchMovies = null;
            var searchMoviesCmdText = "SELECT * FROM movie WHERE CONCAT(`MovieID`, `MovieName`, `MovieActors`, `MovieYear`, `MovieDescription`) like '%" + valueToSearch+"%'";
            var searchMoviesCmd = new MySqlCommand(searchMoviesCmdText, movConnection);

            OpenConnection();
            var reader = searchMoviesCmd.ExecuteReader();

            if (reader.HasRows)
            {
                searchMovies = new List<MovieViewModel>();
            }

            while (reader.Read())
            {
                searchMovies.Add(new MovieViewModel
                {
                    MovieID = reader.GetInt32("MovieID"),
                    MovieName = reader.GetString("MovieName"),
                    MovieActors = reader.GetString("MovieActors"),
                    MovieYear = reader.GetInt32("MovieYear"),
                    MovieDescription = reader.GetString("MovieDescription"),
                    IsAvailable = true
                });
            }
            CloseConnection();
            return searchMovies;
        }
    }
}
