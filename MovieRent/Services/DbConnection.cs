﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DbConnection
    {

        protected MySqlConnection movConnection { get; private set; }

        public DbConnection()
        {
            movConnection = new MySqlConnection(ConfigurationManager.ConnectionStrings["movierentConnectionString"].ConnectionString);
        }
    }
}
