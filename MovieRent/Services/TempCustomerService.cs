﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Services
{
    public class TempCustomerService : BaseService
    {
        public TempCustomerViewModel TempCustomer(TempCustomerViewModel viewModel)
        {
            var CreateTempCustomerCmdText = "INSERT INTO tempCustomer (CustomerID, CustomerName)" +
                " VALUES (@CustomerID, @CustomerName, @CustomerLastName); ";

            var CreateTempCustomerCmd = new MySqlCommand(CreateTempCustomerCmdText, movConnection);
            CreateTempCustomerCmd.Parameters.AddWithValue("@CustomerID", viewModel.CustomerID);
            CreateTempCustomerCmd.Parameters.AddWithValue("@CustomerName", viewModel.CustomerName);
            CreateTempCustomerCmd.Parameters.AddWithValue("@CustomerLastName", viewModel.CustomerLastName);

            OpenConnection();
            CreateTempCustomerCmd.ExecuteNonQuery();
            CloseConnection();

            return viewModel;
        }

        public List<TempCustomerViewModel> GetTempCustomer()
        {
            List<TempCustomerViewModel> tempCustomers = null;
            var customersCmdText = "SELECT * FROM customer;";
            var customersCmd = new MySqlCommand(customersCmdText, movConnection);

            OpenConnection();
            var reader = customersCmd.ExecuteReader();

            if (reader.HasRows)
            {
                tempCustomers = new List<TempCustomerViewModel>();
            }

            while (reader.Read())
            {
                tempCustomers.Add(new TempCustomerViewModel
                {
                    CustomerID = reader.GetInt32("CustomerID"),
                    CustomerName = reader.GetString("CustomerName"),
                    CustomerLastName = reader.GetString("CustomerLastName")
                });
            }
            CloseConnection();
            return tempCustomers;
        }

        public int DeleteTempCustomer(int ID)
        {
            var deleteTempCustomerCmdText = "DELETE * FROM TempCustomer where customerID = @customerID";
            var deleteTempCustomerCmd = new MySqlCommand(deleteTempCustomerCmdText, movConnection);

            //Parameters
            deleteTempCustomerCmd.Parameters.AddWithValue("@customerID", ID);

            OpenConnection();

            return deleteTempCustomerCmd.ExecuteNonQuery();
        }


    }
}
