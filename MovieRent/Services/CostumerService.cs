﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Services
{
    public class CostumerService : BaseService
    {
        public CustomerViewModel CreateCustomer(CustomerViewModel viewModel)
        {
            var CreateCustomerCmdText = "INSERT INTO customer (CustomerID, CustomerName, CustomerLastName, CustomerPhone, CustomerEmail, CustomerAddress)" +
                " VALUES (@CustomerID, @CustomerName, @CustomerLastName, @CustomerPhone, @CustomerEmail, @CustomerAddress); ";

            var CreateCustomerCmd = new MySqlCommand(CreateCustomerCmdText, movConnection);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerID", viewModel.CustomerID);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerName", viewModel.CustomerName);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerLastName", viewModel.CustomerLastName);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerPhone", viewModel.CustomerPhone);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerEmail", viewModel.CustomerEmail);
            CreateCustomerCmd.Parameters.AddWithValue("@CustomerAddress", viewModel.CustomerAddress);

            OpenConnection();
            CreateCustomerCmd.ExecuteNonQuery();
            CloseConnection();

            return viewModel;
        }

        public List<CustomerViewModel> GetCustomer()
        {
            List<CustomerViewModel> customers = null;
            var customersCmdText = "SELECT * FROM customer;";
            var customersCmd = new MySqlCommand(customersCmdText, movConnection);

            OpenConnection();
            var reader = customersCmd.ExecuteReader();

            if (reader.HasRows)
            {
                customers = new List<CustomerViewModel>();
            }

            while (reader.Read())
            {
                customers.Add(new CustomerViewModel
                {
                    CustomerID = reader.GetInt32("CustomerID"),
                    CustomerName = reader.GetString("CustomerName"),
                    CustomerLastName = reader.GetString("CustomerLastName"),
                    CustomerPhone = reader.GetString("CustomerPhone"),
                    CustomerEmail = reader.GetString("CustomerEmail"),
                    CustomerAddress = reader.GetString("CustomerAddress")
                });
            }
            CloseConnection();
            return customers;
        }

        public CustomerViewModel UpdateCustomer(CustomerViewModel viewModel)
        {
            var updateCustomerCmdText = "UPDATE customer SET CustomerName = @CustomerName, CustomerLastName = @CustomerLastName, CustomerPhone = @CustomerPhone, " +
                "CustomerEmail = @CustomerEmail, CustomerAddress = @CustomerAddress WHERE CustomerID = @CustomerID;";

            var updateCustomerCmd = new MySqlCommand(updateCustomerCmdText, movConnection);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerName", viewModel.CustomerName);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerLastName", viewModel.CustomerLastName);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerPhone", viewModel.CustomerPhone);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerEmail", viewModel.CustomerEmail);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerAddress", viewModel.CustomerAddress);
            updateCustomerCmd.Parameters.AddWithValue("@CustomerID", viewModel.CustomerID);

            OpenConnection();
            updateCustomerCmd.ExecuteNonQuery();
            CloseConnection();
            return viewModel;
        }

        public int DeleteCustomer(int ID)
        {
            var deleteCustomerCmdText = "DELETE FROM customer where customerID = @customerID";
            var deleteCustomerCmd = new MySqlCommand(deleteCustomerCmdText, movConnection);

            //Parameters
            deleteCustomerCmd.Parameters.AddWithValue("@customerID", ID);

            OpenConnection();

            return deleteCustomerCmd.ExecuteNonQuery();
        }

        public List<CustomerViewModel> SearchCustomer (string valueToSearch)
        {
            List<CustomerViewModel> searchCostumer = null;
            var searchCostumerCmdText = "SELECT * FROM customer WHERE CONCAT(`CustomerID`, `CustomerName`, `CustomerLastName`, `CustomerPhone`, `CustomerEmail`,  `CustomerAddress`) like '%" + valueToSearch + "%'";
            var searchCostumerCmd = new MySqlCommand(searchCostumerCmdText, movConnection);

            OpenConnection();
            var reader = searchCostumerCmd.ExecuteReader();

            if (reader.HasRows)
            {
                searchCostumer = new List<CustomerViewModel>();
            }

            while (reader.Read())
            {
                searchCostumer.Add(new CustomerViewModel
                {
                    CustomerID = reader.GetInt32("CustomerID"),
                    CustomerName = reader.GetString("CustomerName"),
                    CustomerLastName = reader.GetString("CustomerLastName"),
                    CustomerPhone = reader.GetString("CustomerPhone"),
                    CustomerEmail = reader.GetString("CustomerEmail"),
                    CustomerAddress = reader.GetString("CustomerAddress")
                });
            }
            CloseConnection();
            return searchCostumer;
        }
    }
}
