﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Services
{
    public class WorkerService : BaseService
    {
        public List<WorkerViewModel> GetWorkers()
        {
            List<WorkerViewModel> workers = null;
            var workersCmdText = "SELECT * FROM Worker;";
            var workersCmd = new MySqlCommand(workersCmdText, movConnection);

            OpenConnection();
            var reader = workersCmd.ExecuteReader();

            if (reader.HasRows)
            {
                workers = new List<WorkerViewModel>();
            }

            while (reader.Read())
            {
                workers.Add(new WorkerViewModel
                {
                    WorkerID = reader.GetInt32("workerID"),
                    WorkerName = reader.GetString("workerName"),
                    WorkerLastName = reader.GetString("workerLastName"),
                    WorkerPhone = reader.GetString("workerPhone"),
                    WorkerEmail = reader.GetString("workerEmail")
                });
            }
            CloseConnection();
            return workers;
        }

        public WorkerViewModel CreateWorker(WorkerViewModel viewModel)
        {
            var workerAddCmdText = "INSERT INTO Worker (WorkerID, WorkerName, WorkerLastName, WorkerPhone, WorkerEmail) VALUES (@workerID, " +
                "@workerName, @workerLastName, @workerPhone, @workerEmail);";

            var workerAddCmd = new MySqlCommand(workerAddCmdText, movConnection);
            workerAddCmd.Parameters.AddWithValue("@workerID", viewModel.WorkerID);
            workerAddCmd.Parameters.AddWithValue("@workerName", viewModel.WorkerName);
            workerAddCmd.Parameters.AddWithValue("@workerLastName", viewModel.WorkerLastName);
            workerAddCmd.Parameters.AddWithValue("@workerPhone", viewModel.WorkerPhone);
            workerAddCmd.Parameters.AddWithValue("@workerEmail", viewModel.WorkerEmail);

            OpenConnection();
            workerAddCmd.ExecuteNonQuery();
            CloseConnection();

            return viewModel;
        }

        public WorkerViewModel UpdateWorker(WorkerViewModel viewModel)
        {
            var workerUpdateCmdText = "UPDATE Worker SET WorkerName = @WorkerName, WorkerLastName = @WorkerLastName, WorkerPhone = @WorkerPhone, " +
                "WorkerEmail = @WorkerEmail;";

            var workerUpdateCmd = new MySqlCommand(workerUpdateCmdText, movConnection);
            workerUpdateCmd.Parameters.AddWithValue("@WorkerName", viewModel.WorkerName);
            workerUpdateCmd.Parameters.AddWithValue("@WorkerLastName", viewModel.WorkerLastName);
            workerUpdateCmd.Parameters.AddWithValue("@WorkerPhone", viewModel.WorkerPhone);
            workerUpdateCmd.Parameters.AddWithValue("@WorkerEmail", viewModel.WorkerEmail);

            OpenConnection();
            workerUpdateCmd.ExecuteNonQuery();
            CloseConnection();
            return viewModel;
        }

        public int DeleteWorker(int ID)
        {
            var deleteWorkerCmdText = "DELETE FROM Worker where WorkerID = @workerID";
            var deleteWorkerCmd = new MySqlCommand(deleteWorkerCmdText, movConnection);

            deleteWorkerCmd.Parameters.AddWithValue("@workerID", ID);

            OpenConnection();

            return deleteWorkerCmd.ExecuteNonQuery();
        }

        public List<WorkerViewModel> SearchWorkers(string valueToSearch)
        {
            List<WorkerViewModel> searchWorkers = null;
            var searchWorkersCmdText = "SELECT * FROM Worker WHERE CONCAT(`WorkerID`, `WorkerName`, `WorkerLastName`, `WorkerPhone`, `WorkerEmail`) like '%" + valueToSearch + "%'";
            var searchWorkerCmd = new MySqlCommand(searchWorkersCmdText, movConnection);

            OpenConnection();
            var reader = searchWorkerCmd.ExecuteReader();

            if (reader.HasRows)
            {
                searchWorkers = new List<WorkerViewModel>();
            }

            while (reader.Read())
            {
                searchWorkers.Add(new WorkerViewModel
                {
                    WorkerID = reader.GetInt32("WorkerID"),
                    WorkerName = reader.GetString("WorkerName"),
                    WorkerLastName = reader.GetString("WorkerLastName"),
                    WorkerPhone = reader.GetString("WorkerPhone"),
                    WorkerEmail = reader.GetString("WorkerEmail")
                });
            }
            CloseConnection();
            return searchWorkers;
        }

    }

    
}
